package junitfaq;
import CalculKilom;

import static org.junit.Assert.*;


import org.junit.*;
import java.util.*;

public class CalculKilomTest {
	public void testConstructeur() {
		CalculKilom k = new CalculKilom();
		assertEquals(
	            k.getNbKil(), 0);
		CalculKilom k2 = new CalculKilom(10);
		assertEquals(k.getNbKil(), 10)
	}
	
	public void testCalculs() {
		CalculKilom k = new CalculKilom();
		assertEquals(k.indemnisation(), 0)
		k.setNbKil(5)
		assertEquals(k.indemnisation(), 7.5)
		k.setNbKil(10)
		assertEquals(k.indemnisation(), 15)
		k.setNbKil(20)
		assertEquals(k.indemnisation(), 19)
		k.setNbKil(40)
		assertEquals(k.indemnisation(), 27)
		k.setNbKil(50)
		assertEquals(k.indemnisation(), 32.5)
		k.setNbKil(60)
		assertEquals(k.indemnisation(), 38)
		k.setNbKil(65)
		assertEquals(k.indemnisation(), 38)
		k.setNbKil(79)
		assertEquals(k.indemnisation(), 38)
		k.setNbKil(80)
		assertEquals(k.indemnisation(), 44.81)
		k.setNbKil(79.5)
		assertEquals(k.indemnisation(), 38)
	}
	
}

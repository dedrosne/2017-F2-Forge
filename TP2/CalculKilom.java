



public class CalculKilom {
	public CalculKilom(double nb) {
		nbKil = nb;
	}
	public CalculKilom() {
		nbKil = 0;
	}
	double nbKil;
	public double getNbKil() {
		return nbKil;
	}
	public void setNbKil(double nb) {
		nbKil = nb;
	}
	
	public void main(String[] args) {
		
	}
	
	
	
	double indemnisation(double nbk) {
		double retour = 0;
			retour += Math.min(10.0,nbk)*1.5;
		if (nbk >=10) {
			retour += (Math.min(40,nbk)-10)*0.4;
		}
		if (nbk>=40) {
			retour += (Math.min(nbk,60)-40)*0.55;
		}
		if (nbk < 60) {
			retour += 6.81*((int) (nbk-60)/20);
		}
		return retour;
		
	}
	
	
}
